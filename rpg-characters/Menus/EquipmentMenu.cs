﻿using System.Text;
using rpg_hero_assignment_1.Hero;
using rpg_hero_assignment_1.Items;

namespace rpg_hero_assignment_1.Menus
{
    public class EquipmentMenu : MenuBase
    {
        public override void ShowMenu()
        {
            sb.Clear();

            ShowHeroSelect((result) =>
            {
                if (result == true)
                {
                    ShowInventoryScreen();
                }
                else
                {
                    ReturnToParentMenu();
                }
            });
        }
        
        void ShowInventoryScreen()
        {
            Console.Clear();
            ShowEquippedItems();
            Console.WriteLine("1.Change weapon\n2.Change armor\n\nEsc. Go Back");

            bool selected = false;

            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        ShowEquipScreen(ItemType.Weapon);
                        selected = true;
                        break;
                    case ConsoleKey.D2:
                        ShowEquipScreen(ItemType.Armor);
                        selected = true;
                        break;
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }
        }

        //Display Items if items are equipped
        static void ShowEquippedItems()
        {
            var sb = new StringBuilder();
            sb.Append($"{HeroHandler.GetHeroInfoShort()}\n\n");

            if (HeroHandler.SelectedHero.EquippedArmors.Count > 0 ||
                HeroHandler.SelectedHero.EquippedWeapons.Count > 0)
            {
                foreach (var kvp in HeroHandler.SelectedHero.EquippedArmors)
                {
                    sb.Append($"{kvp.Value.GetFullName()}\n");
                }

                foreach (var kvp in HeroHandler.SelectedHero.EquippedWeapons)
                {
                    sb.Append($"{kvp.Value.GetFullName()}\n");
                }
            }
            else
            {
                sb.Append("Hero doesn't have any items\n");
            }

            Console.WriteLine(sb.ToString());
        }

        void ShowEquipScreen(ItemType itemType)
        {
            Console.Clear();
            var sb = new StringBuilder();
            sb.Append($"{HeroHandler.GetHeroInfoShort()}\n\n {GetEquippableItems(itemType)}\n\n 1.Equip {(itemType == ItemType.Weapon ? "weapon" : "armor")}\n{(itemType == ItemType.Weapon ? "2.Generate new list of weapons" : "2.Generate new list of armor")}\n\nEsc. Return");
            Console.WriteLine(sb.ToString());

            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    //When Item is chosen equip
                    case ConsoleKey.D1:
                        selected = true;
                        switch (itemType)
                        {
                            //Check if ItemType is Weapon or Armor
                            case ItemType.Weapon:
                                EquipWeapon();
                                break;
                            case ItemType.Armor:
                                EquipArmor();
                                break;
                        }

                        break;
                    //Get new list of items
                    case ConsoleKey.D2:
                        ItemHandler.CreateItems(itemType);
                        ShowEquipScreen(itemType);
                        selected = true;
                        break;
                    //Return to previous screen
                    case ConsoleKey.Escape:
                        ShowInventoryScreen();
                        selected = true;
                        break;
                }
            }
        }

        static string GetEquippableItems(ItemType itemType)
        {
            var sb = new StringBuilder();

            //If Item is Armor or Weapon
            switch (itemType)
            {
                //If ItemType is Weapon get list of weapon
                //If no armor is avaiable display message else display Weapons

                case ItemType.Weapon:

                    if (ItemHandler.Weapons.Count == 0)
                    {
                        sb.Append("Please load in weapons.");
                    }
                    else
                    {
                        for (int i = 0; i < ItemHandler.Weapons.Count; ++i)
                        {
                            Weapon w = ItemHandler.Weapons[i];
                            string restriction = "";
                            bool canEquip;
                            try
                            {
                                canEquip = HeroHandler.SelectedHero.CanEquipWeapon(w);
                            }
                            catch (HeroExceptions.ArmorException)
                            {
                                restriction = $"Requires {ItemHandler.GetHeroClassesForItem(w.WeaponType)}.";
                                canEquip = false;
                            }
                            catch (Exception e)
                            {

                                restriction = e.Message;
                                canEquip = false;
                            }
                            
                            sb.Append(String.Format(" {0,0} ", (i+1)));
                            sb.Append(String.Format(" {0,0} ", (w.GetFullName())));
                            sb.Append("lvl." + String.Format(" {0,0} ", (w.RequiredLevel)));
                            if(!canEquip)sb.Append(String.Format(" {0,0}", (restriction)));
                            sb.Append("\n");
                        }
                    }

                    break;

                //If ItemType is Armor get list of armor
                //If no armor is avaiable display message else display armor
                case ItemType.Armor:
                    if (ItemHandler.Armors.Count == 0)
                    {
                        sb.Append("Please load in armor\n");
                    }
                    else
                    {
                        int longestNameLength = ItemHandler.GetLongestName(ItemType.Armor);
                        
                        for (int i = 0; i < ItemHandler.Armors.Count; ++i)
                        {
                            Armor a = ItemHandler.Armors[i];
                            string restriction = "";

                            bool canEquip;
                            try
                            {
                                canEquip = HeroHandler.SelectedHero.CanEquipArmor(a);
                            }
                            catch (HeroExceptions.ArmorException)
                            {
                                restriction = $"Requires {ItemHandler.GetHeroClassesForItem(a.ArmorType)}.";
                                canEquip = false;
                            }
                            catch (Exception e)
                            {
                                restriction = e.Message;
                                canEquip = false;
                            }

                            sb.Append(String.Format("{0,0}", (i+1)));
                            if (longestNameLength < 40)
                            {
                                sb.Append(String.Format(" {0,0} ", (a.GetFullName())));
                            }
                            else
                            {
                                sb.Append(String.Format(" {0,0} ", (a.GetFullName())));
                            }
                            sb.Append("lvl. " + String.Format(" {0,0} ", (a.RequiredLevel)));
                            if(!canEquip)sb.Append(String.Format(" {0,0}", (restriction)));
                            sb.Append("\n");
                        }
                    }
                    break;
            }
            return sb.ToString();
        }

        //Check if armor can be equipped
        //When true equip armor
        void EquipArmor()
        {
            Console.WriteLine("What do you want to equip?");
            bool valid = int.TryParse(Console.ReadLine(), out int index);

            if (valid && index > 0 && index <= ItemHandler.Armors.Count)
            {
                Armor a = ItemHandler.Armors[index - 1];
                ItemHandler.EquipArmor(a, HeroHandler.SelectedHero);
                Thread.Sleep(TimeSpan.FromSeconds(RequiredElements.DialogueDelay));
            }
            else
            {
                Console.WriteLine("Not allowed");
                Thread.Sleep(TimeSpan.FromSeconds(RequiredElements.DialogueDelay));
            }

            ShowEquipScreen(ItemType.Armor);
        }

        //Check if weapon can be equipped
        //When true equip weapon
        void EquipWeapon()
        {
            var sb = new StringBuilder();
            sb.Append("What do you want to equip?");
            Console.WriteLine(sb.ToString());
            sb.Clear();
            bool valid = int.TryParse(Console.ReadLine(), out int index);

            if (valid && index > 0 && index <= ItemHandler.Weapons.Count)
            {
                Weapon w = ItemHandler.Weapons[index - 1];
                ItemHandler.EquipWeapon(w, HeroHandler.SelectedHero);
                Thread.Sleep(TimeSpan.FromSeconds(RequiredElements.DialogueDelay));
            }
            else
            {
                Console.WriteLine("Not allowed");
                Thread.Sleep(TimeSpan.FromSeconds(RequiredElements.DialogueDelay));
            }

            ShowEquipScreen(ItemType.Weapon);
        }

        //Return to Previous Menu
        public override void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}