﻿namespace rpg_hero_assignment_1.Menus
{
    public enum MenuType
    {
        MainMenu,
        AttributesMenu,
        EquipmentMenu
    }
    public class MainMenu : MenuBase
    {
        //Display menu choices "Equipment" and "Hero attributes"
        public override void ShowMenu()
        {
            Console.Clear();
            Console.WriteLine("1.Attributes\n2.Equipment\n\nClose program: Q");
            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.D1:
                    Console.WriteLine("Show attributes");
                    MenuManager.SetMenu(MenuType.AttributesMenu);
                    break;
                case ConsoleKey.D2:
                    Console.WriteLine("Show equipment");
                    MenuManager.SetMenu(MenuType.EquipmentMenu);
                    break;
                case ConsoleKey.Q:
                    ReturnToParentMenu();
                    break;
            }
        }

        public override void ReturnToParentMenu()
        {
            Environment.Exit(0);
        }
    }
}