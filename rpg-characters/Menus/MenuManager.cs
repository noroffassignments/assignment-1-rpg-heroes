﻿using System.Text;

namespace rpg_hero_assignment_1.Menus
{
    public static class MenuManager
    {
        private static MenuType CurrentMenu = MenuType.MainMenu;
        private static readonly MainMenu MainMenu = new();
        private static readonly AttributesMenu AttributesMenu = new();
        private static readonly EquipmentMenu EquipmentMenu = new();

        //Display menu's based on selections
        public static void ShowMenus()
        {
            Console.Clear();
            while (true)
            {
                if (CurrentMenu == MenuType.MainMenu)
                {
                    MainMenu.ShowMenu();
                }
                else if (CurrentMenu == MenuType.AttributesMenu)
                {
                    AttributesMenu.ShowMenu();
                }
                else if (CurrentMenu == MenuType.EquipmentMenu)
                {
                    EquipmentMenu.ShowMenu();
                }
            }
        }

        public static void SetMenu(MenuType newMenu)
        {
            CurrentMenu = newMenu;
        }
    }
}