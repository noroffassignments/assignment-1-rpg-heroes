﻿using rpg_hero_assignment_1.Hero;

namespace rpg_hero_assignment_1.Menus
{
    public class AttributesMenu : MenuBase
    {
        public override void ShowMenu()
        {
            sb.Clear();
        
            ShowHeroSelect((result) =>
            {
                if (result == true)
                {
                    ShowAttributes();
                }
                else
                {
                    ReturnToParentMenu();
                }
            });
        }

        //Display hero attributes based on Level + Items
        //Level up hero
        void ShowAttributes()
        {
            Console.Clear();
            sb.Clear();

            Hero.Hero c = HeroHandler.SelectedHero;
            sb.Append($"{HeroHandler.GetHeroInfoShort()}\n\n");
            sb.Append($"Strength: {c.GetTotalAttribute(HeroAttribute.Strength)} {(c.GetAttributeBoost(HeroAttribute.Strength) > 0 ? $" (+{c.GetAttributeBoost(HeroAttribute.Strength)} from items)" : "")}\n");
            sb.Append($"Dexterity: {c.GetTotalAttribute(HeroAttribute.Dexterity)} {(c.GetAttributeBoost(HeroAttribute.Dexterity) > 0 ? $" (+{c.GetAttributeBoost(HeroAttribute.Dexterity)} from items)" : "")}\n");
            sb.Append($"Intelligence: {c.GetTotalAttribute(HeroAttribute.Intelligence)} {(c.GetAttributeBoost(HeroAttribute.Intelligence) > 0 ? $" (+{c.GetAttributeBoost(HeroAttribute.Intelligence)} from items)" : "")}\n");
            sb.Append($"Damage: {c.TotalDamage()}\n\n");
            sb.Append($"1.Level up\n\nEsc. Go back");
        
            Console.WriteLine(sb.ToString());

            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        selected = true;
                        Console.Clear();
                        HeroHandler.SelectedHero.LevelUp();
                        Console.WriteLine($"{HeroHandler.SelectedHero.Name} reached level {HeroHandler.SelectedHero.CurrentLevel}!");
                        ShowAttributes();
                        break;
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }
        }

        public override void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}