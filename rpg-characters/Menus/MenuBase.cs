﻿using System.Text;
using rpg_hero_assignment_1.Hero;

namespace rpg_hero_assignment_1.Menus
{
    public abstract class MenuBase
    {
        private protected StringBuilder sb = new StringBuilder();
        public abstract void ShowMenu();
        public abstract void ReturnToParentMenu();

        //Show hero selection screen
        protected void ShowHeroSelect(Action<bool> result)
        {
            Console.Clear();

            for (int i = 0; i < HeroHandler.CurrentHeroes.Count; ++i)
            {
                sb.Append($"{i+1}. {HeroHandler.CurrentHeroes[i].CurrentClass} {HeroHandler.CurrentHeroes[i].Name}\n");
            }

            sb.Append("\nEsc. Go back");
            Console.WriteLine(sb.ToString());

            bool selected = false;
        
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        HeroHandler.SelectedHero = HeroHandler.CurrentHeroes[0];
                        selected = true;
                        break;
                    case ConsoleKey.D2:
                        HeroHandler.SelectedHero = HeroHandler.CurrentHeroes[1];
                        selected = true;
                        break;
                    case ConsoleKey.D3:
                        HeroHandler.SelectedHero = HeroHandler.CurrentHeroes[2];
                        selected = true;
                        break;
                    case ConsoleKey.D4:
                        HeroHandler.SelectedHero = HeroHandler.CurrentHeroes[3];
                        selected = true;
                        break;
                    case ConsoleKey.Escape:
                        selected = true;
                        break;
                }
            }
        
            result.Invoke(HeroHandler.SelectedHero != null);
        }
    }
}