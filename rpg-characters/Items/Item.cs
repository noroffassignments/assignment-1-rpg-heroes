﻿using rpg_hero_assignment_1.Hero;

namespace rpg_hero_assignment_1.Items
{
    public enum ItemType
    {
        Weapon,
        Armor
    }
    
    public enum ItemSlot
    {
        Head,
        Body,
        Legs,
        Weapon
    }

    public abstract class Item
    {
        protected string Name;
        public abstract string GetFullName();
        public int RequiredLevel;
        public HeroAttribute? AttributeBoostType;
        public int? AttributeBoostValue = 0;
        protected int Identifier;
        public ItemSlot Slot;

        //Create rang for different items
        public string GetRang()
        {
            if (RequiredLevel <= RequiredElements.NormalItemHighestLevel)
            {
                return "Normal";
            }
            else if (RequiredLevel <= RequiredElements.GoodItemHighestLevel)
            {
                return "Good";
            }
            else if (RequiredLevel <= RequiredElements.SpecialItemHighestLevel)
            {
                return "Special";
            }
            else
            {
                return "Excellent";
            }
        }
    }
}