﻿using rpg_hero_assignment_1.Hero;

namespace rpg_hero_assignment_1.Items
{
    public enum WeaponType
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; }
        public int BaseDamage { get; }
        public float AttacksPerSecond { get; }

        public Weapon(int identifier, string name, int requiredLevel, WeaponType weaponType, int baseDamage,
            float attacksPerSecond, ItemSlot itemSlot, HeroAttribute? attributeBoostType, int? attributeBoostValue)
        {
            Identifier = identifier;
            Name = name;
            RequiredLevel = requiredLevel;
            WeaponType = weaponType;
            BaseDamage = baseDamage;
            AttacksPerSecond = attacksPerSecond;
            Slot = itemSlot;
            AttributeBoostType = attributeBoostType;
            AttributeBoostValue = attributeBoostValue;
        }

        public float DPS => BaseDamage * AttacksPerSecond;
    
        public override string GetFullName()
        {
            return $"{GetRang()} {WeaponType}";
        }
    }
}