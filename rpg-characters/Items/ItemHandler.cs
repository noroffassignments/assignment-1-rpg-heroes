﻿using rpg_hero_assignment_1.Hero;
using System.Dynamic;

namespace rpg_hero_assignment_1.Items
{
    public static class ItemHandler
    {
        public static readonly List<Weapon> Weapons = new List<Weapon>();
        public static readonly List<Armor> Armors = new List<Armor>();
        static readonly Random Rand = new Random();
        private static readonly int WeaponCount = 14;
        private static readonly int ArmorCount = 14;

        //Create Items based on ItemType for displaying list
        public static void CreateItems(ItemType? itemType = null)
        {
            if (itemType == null || itemType == ItemType.Weapon)
            {
                Weapons.Clear();
                for (int i = 1; i <= WeaponCount; ++i)
                {
                    var thisWeaponRequiredLevel = i + Rand.Next(0,2);
                    var attributeBoost = thisWeaponRequiredLevel > RequiredElements.NormalItemHighestLevel
                        ? Rand.Next(1, 4) * i
                        : 0;
                    WeaponType weapon = (WeaponType)(Rand.Next(0, Enum.GetNames(typeof(WeaponType)).Length - 1));
                    Weapons.Add(new Weapon(i, nameof(weapon), thisWeaponRequiredLevel, weapon,Rand.Next(10, 16) * i, Rand.Next(1, 4), ItemSlot.Weapon, GetRandomHeroAttribute(weapon),attributeBoost));
                }
            }

            if (itemType == null || itemType == ItemType.Armor)
            {
                Armors.Clear();
                for (int i = 1; i <= ArmorCount; ++i)
                {
                    var thisArmorRequiredLevel = i + Rand.Next(0,2);
                    var attributeBoost = thisArmorRequiredLevel > RequiredElements.NormalItemHighestLevel ? Rand.Next(0, 4) * i : 0; ArmorType armor = (ArmorType)(Rand.Next(0, Enum.GetNames(typeof(ArmorType)).Length - 1));
                    Armors.Add(new Armor(i, nameof(armor), thisArmorRequiredLevel, armor, (ItemSlot)(i % 3),GetRandomHeroAttribute(armor), attributeBoost));
                }
            }
        }

        public static void EquipArmor(Armor armor, Hero.Hero selectedHero)
        {
            if (selectedHero.EquipArmor(armor))
            {
                Armors.Remove(armor);
            }
            else
            {
                Console.WriteLine(
                    $"Could not equip level {armor.RequiredLevel} {armor.ArmorType} for level {selectedHero.CurrentLevel} {selectedHero.CurrentClass}.");
            }
        }

        //Equip Weapon
        public static void EquipWeapon(Weapon weapon, Hero.Hero selectedHero)
        {
            if (selectedHero.EquipWeapon(ItemSlot.Weapon, weapon))
            {
                Weapons.Remove(weapon);
            }
            else
            {
                Console.WriteLine($"Could not equip {weapon.WeaponType} for level {selectedHero.CurrentLevel} {selectedHero.CurrentClass}.");
            }
        }

        //Get attributes for items 
        private static HeroAttribute? GetRandomHeroAttribute<T>(T itemType)
        {
            var rnd = new Random();
            List<HeroAttribute> allowedAttributes = new();

            switch (itemType)
            {
                case ArmorType.Cloth:
                case WeaponType.Staff:
                case WeaponType.Wand:
                    allowedAttributes.Add(HeroAttribute.Intelligence);
                    break;
                case ArmorType.Leather:
                case WeaponType.Bow:
                case WeaponType.Dagger:
                    allowedAttributes.Add(HeroAttribute.Dexterity);
                    break;
                case ArmorType.Mail:
                case WeaponType.Sword:
                    allowedAttributes.Add(HeroAttribute.Dexterity);
                    allowedAttributes.Add(HeroAttribute.Strength);
                    break;
                case ArmorType.Plate:
                case WeaponType.Axe:
                case WeaponType.Hammer:
                    allowedAttributes.Add(HeroAttribute.Strength);
                    break;
            }

            return allowedAttributes.Count > 0 ? allowedAttributes[rnd.Next(0, allowedAttributes.Count - 1)] : null;
        }

        //Create name of item + add Rang
        public static int GetLongestName(ItemType type)
        {
            int longest = 0;
            switch (type)
            {
                case ItemType.Armor:
                    foreach (Armor a in Armors)
                    {
                        int thisLength = a.GetFullName().Length;
                        if (thisLength > longest)
                        {
                            longest = thisLength;
                        }
                    }
                    break;
                case ItemType.Weapon:
                    foreach (Weapon w in Weapons)
                    {
                        int thisLength = w.GetFullName().Length;
                        if (thisLength > longest)
                        {
                            longest = thisLength;
                        }
                    }
                    break;
            }

            return longest;
        }

        //Get required classes for items
        public static string GetHeroClassesForItem<T>(T itemType)
        {
            string classes = "";
            switch (itemType)
            {
                case ArmorType.Cloth:
                case WeaponType.Staff:
                case WeaponType.Wand:
                    classes = "Mage";
                    break;
                case ArmorType.Plate:
                case WeaponType.Axe:
                case WeaponType.Hammer:
                    classes = "Warrior";
                    break;
                case ArmorType.Mail:
                    classes = "Ranger, Rogue or Warrior";
                    break;
                case ArmorType.Leather:
                    classes = "Ranger or Rogue";
                    break;
                case WeaponType.Sword:
                    classes = "Rogue or Warrior";
                    break;
                case WeaponType.Bow:
                    classes = "Ranger";
                    break;
                case WeaponType.Dagger:
                    classes = "Rogue";
                    break;
            }
            
            return classes;
        }
    }
}