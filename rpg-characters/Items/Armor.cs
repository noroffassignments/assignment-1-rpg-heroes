﻿using rpg_hero_assignment_1.Hero;

namespace rpg_hero_assignment_1.Items
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public int Power { get; }

        public Armor(int identifier, 
            string name, 
            int requiredLevel, 
            ArmorType armorType, 
            ItemSlot itemSlot, 
            HeroAttribute? attributeBoostType, 
            int? attributeBoostValue)
        {
            Identifier = identifier;
            Name = name;
            RequiredLevel = requiredLevel;
            ArmorType = armorType;
            Slot = itemSlot;
            AttributeBoostType = attributeBoostType;
            AttributeBoostValue = attributeBoostValue;
        }

        //Get name for armor piece
        public override string GetFullName()
        {
            return $"{GetRang()} {ArmorType} ({Slot})";
        }
    }
}