﻿namespace rpg_hero_assignment_1;

public class HeroExceptions
{
    //Exception for when a weapon is not allowed to be equipped by a hero of a specific class
    public class WeaponException : Exception
    {
        public override string Message => "This item is not Allowed to be Equipped by this class";
    }

    //Exception for when a piece of armor is not allowed to be equipped by a hero of a specific class
    public class ArmorException : Exception
    {
        public override string Message => "This piece of armor is not Allowed to be Equipped by this class";
    }

    //Exception for when an item is not allowed to be equipped becuase of the  heroes level
    public class ItemLevelException : Exception
    {
        public override string Message => "Your Character's level is not compatible for this item";
    }
}