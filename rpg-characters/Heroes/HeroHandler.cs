﻿namespace rpg_hero_assignment_1.Hero
{
    public static class HeroHandler
    {
        private static Hero selectedHero;

        public static rpg_hero_assignment_1.Hero.Hero SelectedHero { get => selectedHero; set => selectedHero = value; }
        public static string GetHeroInfoShort()
        {
            return $"{SelectedHero.CurrentClass} {SelectedHero.Name} (Level {SelectedHero.CurrentLevel})";
        }

        public static List<rpg_hero_assignment_1.Hero.Hero> CurrentHeroes { get; private set; } = new List<rpg_hero_assignment_1.Hero.Hero>(4);

        //Create heroes + add classes
        public static void CreateHero()
        {
            CurrentHeroes.Add(new HeroClasses.Mage());
            CurrentHeroes.Add(new HeroClasses.Ranger());
            CurrentHeroes.Add(new HeroClasses.Rogue());
            CurrentHeroes.Add(new HeroClasses.Warrior());
        }
    }
}