﻿using rpg_hero_assignment_1.Items;

namespace rpg_hero_assignment_1.Hero
{
    public enum HeroClass
    {
        Mage,
        Ranger,
        Rogue,
        Warrior
    }

    public enum HeroAttribute
    {
        Strength,
        Dexterity,
        Intelligence
    }

    //Create all hero classes with specific Data
    public class HeroClasses
    {

        public class Mage : Hero
        { 
            public Mage()
            {
                CurrentClass = HeroClass.Mage;
            }

            protected override List<WeaponType> AllowedWeaponTypes { get; set; } = new()
            {
                WeaponType.Staff,
                WeaponType.Wand
            };

            protected override List<ArmorType> AllowedArmorTypes { get; set; } = new()
            {
                ArmorType.Cloth
            };

        }

        public class Ranger : Hero
        {
            public Ranger()
            {
                CurrentClass = HeroClass.Ranger;
            }

            protected override List<WeaponType> AllowedWeaponTypes { get; set; } = new()
            {
                WeaponType.Bow
            };

            protected override List<ArmorType> AllowedArmorTypes { get; set; } = new()
            {
                ArmorType.Leather,
                ArmorType.Mail
            };
        }

        public class Rogue : Hero
        {
            public Rogue()
            {
                CurrentClass = HeroClass.Rogue;
            }

            protected override List<WeaponType> AllowedWeaponTypes { get; set; } = new()
            {
                WeaponType.Dagger,
                WeaponType.Sword
            };

            protected override List<ArmorType> AllowedArmorTypes { get; set; } = new()
            {
                ArmorType.Leather,
                ArmorType.Mail
            };

        }

        public class Warrior : Hero
        {
            public Warrior()
            {
                CurrentClass = HeroClass.Warrior;
            }

            protected override List<ArmorType> AllowedArmorTypes { get; set; } = new()
            {
                ArmorType.Mail,
                ArmorType.Plate
            };

            protected override List<WeaponType> AllowedWeaponTypes { get; set; } = new()
            {
                WeaponType.Axe,
                WeaponType.Hammer,
                WeaponType.Sword
            };

        }
    }
}