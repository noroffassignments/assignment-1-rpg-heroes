﻿using rpg_hero_assignment_1.Items;
using rpg_hero_assignment_1.Hero;

namespace rpg_hero_assignment_1.Hero
{
    public static class HeroNamePicker
    {
        //Create Hero Names
        public static void CreateHeroNames()
        {
            for (int i = 0; i < HeroHandler.CurrentHeroes.Count; ++i)
            {
                rpg_hero_assignment_1.Hero.Hero currentHero = HeroHandler.CurrentHeroes[i];

                Console.Clear();

                Console.WriteLine($"Give your name for hero {currentHero.CurrentClass}");
                var selectedName = Console.ReadLine();
                currentHero.Name = selectedName;
                Console.WriteLine($"\nYour Hero with Class {currentHero.CurrentClass} is named {currentHero.Name}.");

                Thread.Sleep(TimeSpan.FromSeconds(RequiredElements.DialogueDelay));
            }
        }
    }
    public abstract class Hero
    {
        public HeroClass CurrentClass { get; protected init; }
        public int CurrentLevel { get; private set; } = 1;

        public void LevelUp(int amount = 1) { CurrentLevel += amount; }
        protected abstract List<WeaponType> AllowedWeaponTypes { get; set; }
        protected abstract List<ArmorType> AllowedArmorTypes { get; set; }
        public Dictionary<ItemSlot, Armor> EquippedArmors { get; private set; } = new Dictionary<ItemSlot, Armor>();
        public Dictionary<ItemSlot, Weapon> EquippedWeapons { get; private set; } = new Dictionary<ItemSlot, Weapon>();

        public string Name { get; set; } = "";

        public bool EquipArmor(Armor armor)
        {
            try
            {
                if (CanEquipArmor(armor))
                {
                    if (EquippedArmors.ContainsKey(armor.Slot))
                    {
                        Console.WriteLine(
                            $"Removing {EquippedArmors.GetValueOrDefault(armor.Slot)?.GetFullName()} from slot {armor.Slot}.");
                        EquippedArmors.Remove(armor.Slot);
                    }

                    EquippedArmors.Add(armor.Slot, armor);

                    Console.WriteLine(
                        $"Adding {EquippedArmors.GetValueOrDefault(armor.Slot)?.GetFullName()} to slot {armor.Slot}.");
                    return true;
                }
            }
            catch(Exception)
            {
                return false;
            }

            return false;
        }
        
        public bool CanEquipArmor(Armor armor)
        {
            if (!AllowedArmorTypes.Contains(armor.ArmorType))
            {
                throw new HeroExceptions.ArmorException();
            }
            else if (armor.RequiredLevel > CurrentLevel)
            {
                throw new HeroExceptions.ItemLevelException();
            }
            else
            {
                return true;
            }
        }
        
        public bool EquipWeapon(ItemSlot itemSlot, Weapon weapon)
        {
            try
            {
                if (CanEquipWeapon(weapon))
                {
                    if (EquippedWeapons.ContainsKey(itemSlot))
                    {
                        Console.WriteLine(
                            $"Removing {EquippedWeapons.GetValueOrDefault(itemSlot)?.GetFullName()} from slot {itemSlot}.");
                        EquippedWeapons.Remove(itemSlot);
                    }

                    EquippedWeapons.Add(itemSlot, weapon);
                    Console.WriteLine(
                        $"Adding {EquippedWeapons.GetValueOrDefault(itemSlot)?.GetFullName()} to slot {itemSlot}.");
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        public bool CanEquipWeapon(Weapon weapon)
        {
            if (!AllowedWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new HeroExceptions.WeaponException();
            }
            else if (weapon.RequiredLevel > CurrentLevel)
            {
                throw new HeroExceptions.ItemLevelException();
            }
            else
            {
                return true;
            }
        }

        public string TotalDamage()
        {
            float totalDamage = 0;
            float dps = 0;
            foreach (var kvp in EquippedWeapons)
            {
                dps += kvp.Value.DPS;
            }

            dps = dps < 1 ? 1 : dps;
            float attributeModifier = 1;
            
            switch (CurrentClass)
            {
                case HeroClass.Mage:
                    attributeModifier = (100 + GetTotalAttribute(HeroAttribute.Intelligence));
                    break;
                case HeroClass.Ranger:
                case HeroClass.Rogue:
                    attributeModifier = (100 + GetTotalAttribute(HeroAttribute.Dexterity));
                    break;
                case HeroClass.Warrior:
                    attributeModifier = (100 + GetTotalAttribute(HeroAttribute.Strength));
                    break;
            }

            totalDamage = (dps * attributeModifier) / (float)100;
            return totalDamage == 1 ? "1" : $"{totalDamage.ToString("0.00")} ({dps.ToString("0.00")} * {(attributeModifier/100.0).ToString("0.00")})";
        }
        
        public float TotalDamageValue()
        {
            float totalDamage = 0;
            float dps = 0;
            foreach (var kvp in EquippedWeapons)
            {
                dps += kvp.Value.DPS;
            }

            dps = dps < 1 ? 1 : dps;
            float attributeModifier = 1;
            
            switch (CurrentClass)
            {
                case HeroClass.Mage:
                    attributeModifier = (100 + GetTotalAttribute(HeroAttribute.Intelligence));
                    break;
                case HeroClass.Ranger:
                case HeroClass.Rogue:
                    attributeModifier = (100 + GetTotalAttribute(HeroAttribute.Dexterity));
                    break;
                case HeroClass.Warrior:
                    attributeModifier = (100 + GetTotalAttribute(HeroAttribute.Strength));
                    break;
            }
            
            totalDamage = (dps * attributeModifier) / (float)100;
            return MathF.Round(totalDamage * 100f) / 100f;
        }

        public int GetAttributeBoost(HeroAttribute selectedAttribute)
        {
            int? armorBoost = EquippedArmors.Where(armor => armor.Value.AttributeBoostType == selectedAttribute)
                .Sum(armor => armor.Value.AttributeBoostValue);
            
            int? weaponBoost = EquippedWeapons.Where(weapon => weapon.Value.AttributeBoostType == selectedAttribute)
                .Sum(weapon => weapon.Value.AttributeBoostValue);

            var totalArmor = armorBoost ?? 0;
            var totalWeapon = weaponBoost ?? 0;

            return totalArmor + totalWeapon;
        }
        
        public int GetBaseAttribute(HeroAttribute attribute)
        {
            switch (CurrentClass)
            {
                case HeroClass.Mage:
                    return attribute switch
                    {   
                        HeroAttribute.Strength => RequiredElements.MageStrength,
                        HeroAttribute.Dexterity => RequiredElements.MageDexterity,
                        HeroAttribute.Intelligence => RequiredElements.MageIntelligence,
                        _ => 0
                    };
                case HeroClass.Ranger:
                    return attribute switch
                    {
                        HeroAttribute.Strength => RequiredElements.RangerStrength,
                        HeroAttribute.Dexterity => RequiredElements.RangerDexterity,
                        HeroAttribute.Intelligence => RequiredElements.RangerIntelligence,
                        _ => 0
                    };
                case HeroClass.Rogue:
                    return attribute switch
                    {
                        HeroAttribute.Strength => RequiredElements.RogueStrength,
                        HeroAttribute.Dexterity => RequiredElements.RogueDexterity,
                        HeroAttribute.Intelligence => RequiredElements.RogueIntelligence,
                        _ => 0
                    };
                case HeroClass.Warrior:
                    return attribute switch
                    {
                        HeroAttribute.Strength => RequiredElements.WarriorStrength,
                        HeroAttribute.Dexterity => RequiredElements.WarriorDexterity,
                        HeroAttribute.Intelligence => RequiredElements.WarriorIntelligence,
                        _ => 0
                    };
                default:
                    return 0;
            }
        }
 
        public int GetTotalAttribute(HeroAttribute attribute)
        {
            return (CurrentClass, attribute) switch
            {
                (HeroClass.Mage, HeroAttribute.Intelligence) => 
                    GetBaseAttribute(HeroAttribute.Intelligence)
                    + (CurrentLevel - 1) * RequiredElements.MageIntelligenceLevelUp
                    + GetAttributeBoost(HeroAttribute.Intelligence),
                (HeroClass.Mage, HeroAttribute.Dexterity) => 
                    GetBaseAttribute(HeroAttribute.Dexterity)
                    + (CurrentLevel - 1) * RequiredElements.MageDexterityLevelUp
                    + GetAttributeBoost(HeroAttribute.Dexterity),
                (HeroClass.Mage, HeroAttribute.Strength) => 
                    GetBaseAttribute(HeroAttribute.Strength)
                    + (CurrentLevel - 1) * RequiredElements.MageStrengthLevelUp
                    + GetAttributeBoost(HeroAttribute.Strength),
                
                (HeroClass.Ranger, HeroAttribute.Intelligence) => 
                    GetBaseAttribute(HeroAttribute.Intelligence)
                    + (CurrentLevel - 1) * RequiredElements.RangerIntelligenceLevelUp
                    + GetAttributeBoost(HeroAttribute.Intelligence),
                (HeroClass.Ranger, HeroAttribute.Dexterity) => 
                    GetBaseAttribute(HeroAttribute.Dexterity)
                    + (CurrentLevel - 1) * RequiredElements.RangerDexterityLevelUp
                    + GetAttributeBoost(HeroAttribute.Dexterity),
                (HeroClass.Ranger, HeroAttribute.Strength) => 
                    GetBaseAttribute(HeroAttribute.Strength)
                    + (CurrentLevel - 1) * RequiredElements.RangerStrengthLevelUp
                    + GetAttributeBoost(HeroAttribute.Strength),
                
                (HeroClass.Rogue, HeroAttribute.Intelligence) => 
                    GetBaseAttribute(HeroAttribute.Intelligence)
                    + (CurrentLevel - 1) * RequiredElements.RogueIntelligenceLevelUp
                    + GetAttributeBoost(HeroAttribute.Intelligence),
                (HeroClass.Rogue, HeroAttribute.Dexterity) => 
                    GetBaseAttribute(HeroAttribute.Dexterity)
                    + (CurrentLevel - 1) * RequiredElements.RogueDexterityLevelUp
                    + GetAttributeBoost(HeroAttribute.Dexterity),
                (HeroClass.Rogue, HeroAttribute.Strength) => 
                    GetBaseAttribute(HeroAttribute.Strength)
                    + (CurrentLevel - 1) * RequiredElements.RogueStrengthLevelUp
                    + GetAttributeBoost(HeroAttribute.Strength),
                
                (HeroClass.Warrior, HeroAttribute.Intelligence) => 
                    GetBaseAttribute(HeroAttribute.Intelligence)
                    + (CurrentLevel - 1) * RequiredElements.WarriorIntelligenceLevelUp
                    + GetAttributeBoost(HeroAttribute.Intelligence),
                (HeroClass.Warrior, HeroAttribute.Dexterity) => 
                    GetBaseAttribute(HeroAttribute.Dexterity)
                    + (CurrentLevel - 1) * RequiredElements.WarriorDexterityLevelUp
                    + GetAttributeBoost(HeroAttribute.Dexterity),
                (HeroClass.Warrior, HeroAttribute.Strength) => 
                    GetBaseAttribute(HeroAttribute.Strength)
                    + (CurrentLevel - 1) * RequiredElements.WarriorStrengthLevelUp
                    + GetAttributeBoost(HeroAttribute.Strength),
                
                _ => 0
            };
        }
    }
}