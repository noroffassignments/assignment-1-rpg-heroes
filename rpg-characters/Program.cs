﻿using rpg_hero_assignment_1.Hero;
using rpg_hero_assignment_1.Items;
using rpg_hero_assignment_1.Menus;

namespace rpg_hero_assignment_1
{
    internal static class Program
    {
        public static void Main()
        {
            //Start Hero creation process
            HeroHandler.CreateHero();
            HeroNamePicker.CreateHeroNames();
            ItemHandler.CreateItems();
            MenuManager.ShowMenus();
        }
    }
}