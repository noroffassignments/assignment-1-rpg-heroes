﻿using System.Drawing;

namespace rpg_hero_assignment_1
{
    public class RequiredElements
    {
        //Required Elements that are used throughout most methods and classes
        //Such as Highest Level of an item + instances of level up Heroes

        public const float DialogueDelay = 1.5f;

        public const int NormalItemHighestLevel = 4;
        public const int GoodItemHighestLevel = 8;
        public const int SpecialItemHighestLevel = 12;

        public const int MageStrength = 1;
        public const int MageDexterity = 1;
        public const int MageIntelligence = 8;
        public const int MageStrengthLevelUp = 1;
        public const int MageDexterityLevelUp = 1;
        public const int MageIntelligenceLevelUp = 5;

        public const int RangerStrength = 1;
        public const int RangerDexterity = 7;
        public const int RangerIntelligence = 1;
        public const int RangerStrengthLevelUp = 1;
        public const int RangerDexterityLevelUp = 5;
        public const int RangerIntelligenceLevelUp = 1;

        public const int RogueStrength = 2;
        public const int RogueDexterity = 6;
        public const int RogueIntelligence = 1;
        public const int RogueStrengthLevelUp = 1;
        public const int RogueDexterityLevelUp = 4;
        public const int RogueIntelligenceLevelUp = 1;

        public const int WarriorStrength = 5;
        public const int WarriorDexterity = 2;
        public const int WarriorIntelligence = 1;
        public const int WarriorStrengthLevelUp = 3;
        public const int WarriorDexterityLevelUp = 2;
        public const int WarriorIntelligenceLevelUp = 1;
    }
}